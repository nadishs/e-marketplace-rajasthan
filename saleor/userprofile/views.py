from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse
from django.utils.translation import pgettext
from .forms import (ChangePasswordForm, get_address_form,
                    logout_on_password_change)

import unirest

@login_required
def pathfind(request):
	
	response1 = unirest.get("https://apitest.sewadwaar.rajasthan.gov.in/app/live/Service/hofAndMember/ForApp/WDYYYGG", headers={ "Accept": "application/json" }, params={"client_id": "ad7288a4-7764-436d-a727-783a977f1fe1"})
	bhamid1 = "WDYYYGG"
	place1= response1.body['family_Details'][1]['DISTRICT']


	response2 = unirest.get("https://apitest.sewadwaar.rajasthan.gov.in/app/live/Service/hofAndMember/ForApp/WDBFUUK", headers={ "Accept": "application/json" }, params={"client_id": "ad7288a4-7764-436d-a727-783a977f1fe1"})

	place2= response2.body['family_Details'][1]['DISTRICT']

	bhamid2 = "WDBFUUK"
	distance = unirest.get("https://maps.googleapis.com/maps/api/distancematrix/json", headers={ "Accept": "application/json" }, params={"origins":place1,"destinations":place2,"mode":"driving","key":"AIzaSyDBJEkCRbdlkGOuMPfdvvKe6nL5bwW3NDY"})

	dur= distance.body["rows"][0]["elements"][0]["duration"]["text"]
	dist= distance.body["rows"][0]["elements"][0]["distance"]["text"]
	
	'''
	
	response1 = unirest.get("https://apitest.sewadwaar.rajasthan.gov.in/app/live/Service/hofAndMember/ForApp/WDYYYGG", headers={ "Accept": "application/json" }, params={"client_id": "ad7288a4-7764-436d-a727-783a977f1fe1"})

	place1= response1.body["family_Details"][1]["DISTRICT"]
	#place1= response1.body
	
	response2 = unirest.get("https://apitest.sewadwaar.rajasthan.gov.in/app/live/Service/hofAndMember/ForApp/WD	BFUUK", headers={ "Accept": "application/json" }, params={"client_id": "ad7288a4-7764-436d-a727-783a977f1fe1"})

	place2= response2.body["family_Details"][1]["DISTRICT"]
	#place2= response2.body
	distance = unirest.get("https://maps.googleapis.com/maps/api/distancematrix/json", headers={ "Accept": "application/json" }, params={"origins":place1,"destinations":place2,"mode":"driving","key":"AIzaSyDBJEkCRbdlkGOuMPfdvvKe6nL5bwW3NDY"})

	dis = distance.body["rows"][0]["elements"][0]["duration"]["text"]
	#dis = distance.body
	'''
	ctx = {
		'dis':dur,
		'place1':place1,
		'place2':place2,
		'bhamid1':bhamid1,
		'bhamid2':bhamid2,
		'dist':dist,
		}
    	
    	return TemplateResponse(request, 'userprofile/pathfind.html', ctx)
    	
@login_required
def sell(request):
	current_user = request.user
	if current_user.account_type == 'Seller':
		return TemplateResponse(request, 'userprofile/sell.html')
	else:
		return redirect('/')


@login_required
def details(request):
    password_form = get_or_process_password_form(request)
    ctx = {'addresses': request.user.addresses.all(),
           'orders': request.user.orders.prefetch_related('groups__items'),
           'change_password_form': password_form}

    return TemplateResponse(request, 'userprofile/details.html', ctx)


def get_or_process_password_form(request):
    form = ChangePasswordForm(data=request.POST or None, user=request.user)
    if form.is_valid():
        form.save()
        logout_on_password_change(request, form.user)
        messages.success(request, pgettext(
            'Storefront message', 'Password successfully changed.'))
    return form


@login_required
def address_edit(request, pk):
    address = get_object_or_404(request.user.addresses, pk=pk)
    address_form, preview = get_address_form(
        request.POST or None, instance=address,
        country_code=address.country.code)
    if address_form.is_valid() and not preview:
        address_form.save()
        message = pgettext('Storefront message', 'Address successfully updated.')
        messages.success(request, message)
        return HttpResponseRedirect(reverse('profile:details') + '#addresses')
    return TemplateResponse(
        request, 'userprofile/address-edit.html',
        {'address_form': address_form})


@login_required
def address_delete(request, pk):
    address = get_object_or_404(request.user.addresses, pk=pk)
    if request.method == 'POST':
        address.delete()
        messages.success(
            request,
            pgettext('Storefront message', 'Address successfully deleted.'))
        return HttpResponseRedirect(reverse('profile:details') + '#addresses')
    return TemplateResponse(
        request, 'userprofile/address-delete.html', {'address': address})
        


